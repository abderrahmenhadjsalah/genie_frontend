export class Paper{
  id:number;
  name: string;
  description: string;
  link: string;
  githubLink: string;
  authorsDetails: string;
  publishYear: string;

  constructor(Id, Name, Description, Link, GithubLink, AuthorDetails, PublishYear) {
    let paper = {
      id: Id,
      name: Name,
      description: Description,
      link:Link,
      githubLink:GithubLink,
      authorsDetails:AuthorDetails,
      publishYear:PublishYear
    }
    return paper;
  }
}
