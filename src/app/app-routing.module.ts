import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginPageComponent} from "./login/login-page/login-page.component";
import {ResearchPapersPageComponent} from "./research-papers/pages/research-papers-page/research-papers-page.component";
import {ResearchPapersDetailsPageComponent} from "./research-papers/pages/research-papers-details-page/research-papers-details-page.component";

const routes: Routes = [
  { path: '', component: LoginPageComponent },
  { path: 'papers', component: ResearchPapersPageComponent },
  { path: 'details/:id', component: ResearchPapersDetailsPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
